Simple script that allows to build a library for godot plugin developlment and extract all headers.

## How to use
1. In your plugin repository checkout this repository, yous should get directory called "godotPlugin"
1. cd godotPlugin and run scripts/setup.sh
1. In your project add following to CMakeLists

### CMakeList in your project

~~~~

target_include_directories(yourProject
        PRIVATE
        godotPlugin/godot-cpp/godot_headers/
        godotPlugin/godot-cpp/include/core
        godotPlugin/godot-cpp/include
)

target_link_libraries(yourProject
        PRIVATE
        ${CMAKE_HOME_DIRECTORY}/godotPlugin/bin/linux/libgodot-cpp.linux.64.a
)
~~~~
