# Android.mk
GDNATIVE_LOCAL_PATH := $(call my-dir)
#traverse all the directory and subdirectory
define walk
  $(wildcard $(1)) $(foreach e, $(wildcard $(1)/*), $(call walk, $(e)))
endef

include $(CLEAR_VARS)
LOCAL_MODULE := gdnative_android
LOCAL_CPPFLAGS := -fPIC -g -std=c++14 -Wwrite-strings
LOCAL_SRC_FILES := $(filter %.cpp, $(call walk, $(GDNATIVE_LOCAL_PATH)/../../godot-cpp/src))
LOCAL_C_INCLUDES := \
$(GDNATIVE_LOCAL_PATH)/../../godot-cpp/godot_headers \
$(GDNATIVE_LOCAL_PATH)/../../godot-cpp/include \
$(GDNATIVE_LOCAL_PATH)/../../godot-cpp/include/core
LOCAL_EXPORT_C_INCLUDES := LOCAL_C_INCLUDES
include $(BUILD_STATIC_LIBRARY)