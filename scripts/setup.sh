#!/bin/bash

echo "##### SETUP GODOTNATIVE #####"

sudo apt-get install scons clang

pushd ..

git clone https://github.com/GodotNativeTools/godot-cpp

pushd godot-cpp
git checkout master
git pull
git submodule init
git submodule update
popd

pushd godot-cpp
scons generate_bindings=yes
popd

popd

echo "###################################"
